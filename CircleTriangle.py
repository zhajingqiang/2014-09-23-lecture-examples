#import class modulus
import circle as cc
import triangle as tc

#instantiate a circle and a triangle object
circ=cc.Circle()
tri=tc.Triangle()
#initialize attributes
tri.height=1
tri.width=0.5
circ.radius=2
#compute/print areas for the two objects
print circ.getArea()
print tri.getArea()